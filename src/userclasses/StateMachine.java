/**
 * Your application code goes here
 */

package userclasses;

import generated.StateMachineBase;
import com.codename1.ui.*; 
import com.codename1.ui.events.*;
import com.codename1.ui.table.Table;
import com.codename1.ui.util.Resources;
import com.codename1.xml.Element;
import java.util.Vector;
import userclasses.util.modele.ModeleDeTableau;
import userclasses.util.parser.RequetePourArbreXML;
import static userclasses.util.parser.ParserArbreXML.getValeur;
import userclasses.util.parser.RequetePourTexteBrut;
/**
 *
 * @author Your name here
 */
public class StateMachine extends StateMachineBase {
    
       private RequetePourArbreXML req;
       private RequetePourTexteBrut  req2;
       private String str;
       private Element      racine;
       private Table tableau;
       private ModeleDeTableau model;
       
    public StateMachine(String resFile) {
        super(resFile);
        // do not modify, write code in initVars and initialize class members there,
        // the constructor might be invoked too late due to race conditions that might occur
    }

    @Override
    protected void postMain(Form f) {
        super.postMain(f);

    }

    @Override
    protected void onMain_ButtonAction(Component c, ActionEvent event) {
        super.onMain_ButtonAction(c, event);
        
        String id = this.findTextField().getText();
        String mdp = this.findTextField1().getText();
        
        req2 = new RequetePourTexteBrut();
        req2.executer("gc/user/"+id+"/"+mdp);
        
        str = req2.getTexte();
        
        Label lab ;
        lab = this.findLabel2();
        lab.setText("Connexion : "+str);

        
        if("Ok".equals(str))
        {
               afficherTableau();
        }
        else
        {
               viderLeTableau();
        }
      
        
        
    }
    
    public void afficherTableau(){
        
            req = new RequetePourArbreXML();

            tableau = this.findTable();
            req.executer("gc/resumesregion/tous");
            racine = req.getRacine();
            model = new ModeleDeTableau("codereg","nomreg","caAnnuel","nbclient");

            tableau.setModel(model);

            Vector<Element> lesRegions = racine.getChildrenByTagName("dtoregion");

            for(Element region : lesRegions){

                model.ajouterRangee(getValeur(region, "codereg"),getValeur(region, "nomreg"),getValeur(region, "caannuel"),getValeur(region, "nbclient"));


            }
            tableau.setModel(model);
    }
    
    public void viderLeTableau(){
        
            req = new RequetePourArbreXML();

            tableau = this.findTable();
            req.executer("gc/resumesregion/tous");
            racine = req.getRacine();
            model = new ModeleDeTableau("codereg","nomreg","caAnnuel","nbclient");

            tableau.setModel(model);

            Vector<Element> lesRegions = racine.getChildrenByTagName("dtoregion");

            for(Element region : lesRegions){

                model.ajouterRangee(getValeur(region, "codereg"),getValeur(region, "nomreg"),getValeur(region, "caannuel"),getValeur(region, "nbclient"));


            }
            tableau.setModel(model);
            
            model.viderData();
            tableau.setModel(model);
    }
    
    

}
